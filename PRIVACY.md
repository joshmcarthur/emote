We collect personal information from you, including information about your:

* Emotional classifications

We collect your personal information in order to:

* record and show a timeline of change in recorded emotion over time

We keep your information safe by storing it locally on your device, with no internet or cloud integrations .

You have the right to ask for a copy of any personal information we hold about you, and to ask for it to be corrected if you think it is wrong. If you’d like to ask for a copy of your information, or to have it corrected, please contact us at joshua.mcarthur@gmail.com.
