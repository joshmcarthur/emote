INSERT INTO checkins (_id, emotion_id, created_at)
WITH RECURSIVE fake_checkin(id, emotion_id, created_at) AS (
VALUES(100000, 1, null)
UNION
SELECT id+1, (SELECT _id FROM emotions WHERE _id != emotion_id ORDER BY RANDOM() LIMIT 1), 
                datetime(strftime('%s', '2020-01-01 00:00:00') +
                abs(random() % (strftime('%s', '2020-01-01 00:00:00') -
                                strftime('%s', '2020-06-01 00:00:00'))
                   ),
                'unixepoch') as created_at from fake_checkin
) 
SELECT * from fake_checkin WHERE created_at IS NOT NULL LIMIT 100;
