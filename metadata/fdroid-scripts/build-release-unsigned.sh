#!/bin/bash

# https://github.com/flutter/flutter/issues/44796
# Flutter expects to find APKs at hardcoded paths.
# Because F-Droid rewrites the release build to produce an unsigned build,
# the built APK isn't where Flutter expects it to be.
#
# The below configuration ensures that the release filename remains the same
# regardless of what kind of output has been requested.

echo "
 android {
  buildTypes {
    release {
      applicationVariants.all { variant ->
        variant.outputs.all { output ->
            output.outputFileName = \"app-release.apk\"
        }
      }
    }
  }
}
" >> android/app/build.gradle
