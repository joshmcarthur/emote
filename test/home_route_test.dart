import 'package:emote/home_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Displays FAB', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: HomeRoute()));
    expect(find.byType(FloatingActionButton), findsOneWidget);
  });

  testWidgets('Displays top app bar transparently',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(home: HomeRoute()));
    AppBar bar = tester.widget(find.byType(AppBar));

    expect(bar.elevation, equals(0));
    expect(bar.backgroundColor, equals(Colors.transparent));
  });

  testWidgets("Displays settings button", (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(home: HomeRoute()));
    IconButton settingsButton =
        tester.widget(find.byKey(Key("home__settings_button")));
    Icon settingsIcon = settingsButton.icon as Icon;
    expect(settingsIcon.semanticLabel, equals("Settings"));
  });

  testWidgets("Displays analytics button", (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(home: HomeRoute()));
    IconButton settingsButton =
        tester.widget(find.byKey(Key("home__analytics_button")));
    Icon settingsIcon = settingsButton.icon as Icon;
    expect(settingsIcon.semanticLabel, equals("Analytics"));
  });
}
