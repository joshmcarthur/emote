import 'package:emote/checkin_repository.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("Checkin", () {
    test("builds instance from database map", () {
      var row = {
        "_id": 1,
        "emotion_id": 1,
        "notes": "Lorem ipsum",
        "ignored_column": true,
        "created_at": "1970-01-01 00:00:00"
      };

      Checkin c = Checkin.fromMap(row);

      expect(c.id, equals(1));
      expect(c.emotionId, equals(1));
      expect(c.notes, equals("Lorem ipsum"));
      expect(c.createdAt, equals(new DateTime.utc(1970)));
    });

    test("serializes instance to map", () {
      Checkin c = new Checkin();
      c.id = 1;
      c.emotionId = 1;
      c.notes = "Lorem ipsum";

      var row = {"_id": 1, "emotion_id": 1, "notes": "Lorem ipsum"};

      expect(c.toMap(), equals(row));
    });
  });
}
