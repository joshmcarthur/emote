import 'package:emote/checkin_emotion.dart';
import 'package:emote/checkin_supplementary.dart';
import 'package:emote/emotion_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

class TestEmotionResolver extends EmotionResolver {
  bool resolvedSecondaries = false;
  List<Emotion> primaries, secondaries, tertiaries = [];

  TestEmotionResolver(
      [this.primaries = const [],
      this.secondaries = const [],
      this.tertiaries = const []]);

  @override
  Future<List<Emotion>> childrenOf(int parentId) {
    if (!resolvedSecondaries) {
      resolvedSecondaries = true;
      return Future.value(secondaries);
    } else {
      return Future.value(tertiaries);
    }
  }

  @override
  Future<List<Emotion>> root() => Future.value(primaries);
}

void main() {
  testWidgets('Displays app bar', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(
        home: CheckinEmotionRoute(resolver: TestEmotionResolver())));
    expect(find.text("How are you feeling?"), findsOneWidget);
  });

  testWidgets('Initially only displays top-level emotions',
      (WidgetTester tester) async {
    var primaries = [
      Emotion("Primary 1", "#ffffff"),
      Emotion("Primary 2", "#ffffff")
    ];
    var secondaries = [Emotion("Secondary 1", "#ffffff")];
    var resolver = TestEmotionResolver(primaries, secondaries);

    await tester
        .pumpWidget(MaterialApp(home: CheckinEmotionRoute(resolver: resolver)));
    await tester.pump();
    expect(find.text("Primary 1"), findsOneWidget);
    expect(find.text("Primary 2"), findsOneWidget);
    expect(find.text("Secondary 1"), findsNothing);
  });

  testWidgets('Shows secondary emotions', (WidgetTester tester) async {
    var primaries = [
      Emotion("Primary 1", "#ffffff"),
      Emotion("Primary 2", "#ffffff")
    ];
    var secondaries = [Emotion("Secondary 1", "#ffffff")];
    var resolver = TestEmotionResolver(primaries, secondaries);

    await tester
        .pumpWidget(MaterialApp(home: CheckinEmotionRoute(resolver: resolver)));
    await tester.pump();
    await tester.tap(find.text("Primary 1"));
    await tester.pumpAndSettle();

    expect(find.text("Secondary 1"), findsOneWidget);
  });

  testWidgets('Shows tertiary emotions', (WidgetTester tester) async {
    var primaries = [Emotion("Primary 1", "#ffffff")];
    var secondaries = [Emotion("Secondary 1", "#ffffff")];
    var tertiaries = [Emotion("Tertiary 1", "#ffffff")];
    var resolver = TestEmotionResolver(primaries, secondaries, tertiaries);
    await tester
        .pumpWidget(MaterialApp(home: CheckinEmotionRoute(resolver: resolver)));
    await tester.pump();
    await tester.tap(find.text("Primary 1"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("Secondary 1"));
    await tester.pumpAndSettle();

    expect(find.text("Tertiary 1"), findsOneWidget);
  });

  testWidgets('Selects primary emotion', (WidgetTester tester) async {
    var primaries = [
      Emotion("Primary 1", "#ffffff"),
      Emotion("Primary 2", "#ffffff")
    ];
    var resolver = TestEmotionResolver(primaries);

    await tester
        .pumpWidget(MaterialApp(home: CheckinEmotionRoute(resolver: resolver)));
    await tester.pump();
    await tester.tap(find.text('Primary 2'));
    await tester.pump();
    await tester.tap(find.text("Continue"));
    await tester.pumpAndSettle();
    CheckinSupplementaryRoute target =
        tester.widget(find.byType(CheckinSupplementaryRoute))
            as CheckinSupplementaryRoute;
    expect(target.emotion.label, equals('Primary 2'));
  });

  testWidgets('Selects secondary emotion', (WidgetTester tester) async {
    var primaries = [
      Emotion("Primary 1", "#ffffff"),
      Emotion("Primary 2", "#ffffff")
    ];
    var secondaries = [Emotion("Secondary 1", "#ffffff")];
    var resolver = TestEmotionResolver(primaries, secondaries);

    await tester
        .pumpWidget(MaterialApp(home: CheckinEmotionRoute(resolver: resolver)));
    await tester.pump();
    await tester.tap(find.text("Primary 1"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("Secondary 1"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("Continue"));
    await tester.pumpAndSettle();
    CheckinSupplementaryRoute target =
        tester.widget(find.byType(CheckinSupplementaryRoute))
            as CheckinSupplementaryRoute;
    expect(target.emotion.label, equals('Secondary 1'));
  });

  testWidgets('Selects tertiary emotion', (WidgetTester tester) async {
    var primaries = [
      Emotion("Primary 1", "#ffffff"),
      Emotion("Primary 2", "#ffffff")
    ];
    var secondaries = [Emotion("Secondary 1", "#ffffff")];
    var tertiaries = [Emotion("Tertiary 1", "#ffffff")];

    var resolver = TestEmotionResolver(primaries, secondaries, tertiaries);

    await tester
        .pumpWidget(MaterialApp(home: CheckinEmotionRoute(resolver: resolver)));
    await tester.pump();
    await tester.tap(find.text("Primary 1"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("Secondary 1"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("Tertiary 1"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("Continue"));
    await tester.pumpAndSettle();
    CheckinSupplementaryRoute target =
        tester.widget(find.byType(CheckinSupplementaryRoute))
            as CheckinSupplementaryRoute;
    expect(target.emotion.label, equals('Tertiary 1'));
  });
}
