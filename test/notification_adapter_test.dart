import 'package:emote/notification_adaptor.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("NotificationAdapter", () {
    test("returns a FlutterLocalNotificationsPlugin", () {
      expect(NotificationAdapter.notifications,
          isInstanceOf<FlutterLocalNotificationsPlugin>());
    });

    test("is memoized", () {
      var notifications = NotificationAdapter.notifications;
      var notifications2 = NotificationAdapter.notifications;

      expect(notifications, same(notifications2));
    });
  });
}
