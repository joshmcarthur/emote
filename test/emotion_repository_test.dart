import 'package:emote/emotion_repository.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("Emotion", () {
    test("builds instance from database map", () {
      var row = {
        "_id": 1,
        "label": "Test emotion",
        "colour": "#ffffff",
        "depth": 0,
        "created_at": "1970-01-01 00:00:00"
      };

      Emotion e = Emotion.fromMap(row);

      expect(e.id, equals(1));
      expect(e.label, equals("Test emotion"));
      expect(e.colour, equals("#ffffff"));
    });

    test("serializes instance to map", () {
      Emotion e = new Emotion(null, null, null, null);
      e.id = 1;
      e.colour = "#ffffff";
      e.depth = 0;
      e.parentId = 1;
      e.label = "Lorem ipsum";

      var row = {
        "_id": 1,
        "depth": 0,
        "parent_id": 1,
        "label": "Lorem ipsum",
        "colour": "#ffffff"
      };

      expect(e.toMap(), equals(row));
    });
  });
}
