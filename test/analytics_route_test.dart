import 'package:emote/analytics/analytics_day_of_week.dart';
import 'package:emote/analytics/analytics_four_week_trend_chart.dart';
import 'package:emote/analytics_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Displays app bar', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: AnalyticsRoute()));
    expect(find.text("Analytics"), findsOneWidget);
  });

  testWidgets("Has a four week trend chart shown by default",
      (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: AnalyticsRoute()));
    await tester.pump();
    expect(find.byType(AnalyticsTrendChartFourWeek), findsOneWidget);
    expect(find.byType(AnalyticsTrendChartDayOfWeek), findsNothing);
  });

  testWidgets("Has a 6 month trend chart tab", (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: AnalyticsRoute()));
    expect(find.text("6 months"), findsOneWidget);
  });

  testWidgets("Has a 12 month trend chart tab", (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: AnalyticsRoute()));
    expect(find.text("12 months"), findsOneWidget);
  });

  testWidgets("Has a day of week chart tab", (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: AnalyticsRoute()));
    expect(find.text("Weekday"), findsOneWidget);
  });
}
