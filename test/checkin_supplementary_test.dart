// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:emote/checkin_supplementary.dart';
import 'package:emote/emotion_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Displays app bar', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    Emotion emote = new Emotion("Test", "#FFFFFF", 0, null);
    await tester.pumpWidget(
        MaterialApp(home: CheckinSupplementaryRoute(emotion: emote)));
    expect(find.text("Anything to add?"), findsOneWidget);
  });

  testWidgets('Submits form with no notes', (WidgetTester tester) async {
    Emotion emote = new Emotion("Test", "#FFFFFF", 0, null);
    await tester.pumpWidget(
        MaterialApp(home: CheckinSupplementaryRoute(emotion: emote)));
    await tester.tap(find.text("Add to your timeline"));
    await tester.pump();
    expect(find.text("Adding to your timeline..."), findsOneWidget);
  });

  testWidgets('Submits form with notes', (WidgetTester tester) async {
    Emotion emote = new Emotion("Test", "#FFFFFF", 0, null);
    await tester.pumpWidget(
        MaterialApp(home: CheckinSupplementaryRoute(emotion: emote)));
    await tester.enterText(find.byType(TextFormField), "My notes are here");
    await tester.tap(find.text("Add to your timeline"));
    await tester.pump();
    expect(find.text("Adding to your timeline..."), findsOneWidget);
  });
}
