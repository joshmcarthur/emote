@Skip(
    "Tests requiring SQLite cannot run on machine, run on device with `flutter run`")

import 'package:emote/database_adapter.dart';
import 'package:emote/emotion_repository.dart';
import 'package:flutter_test/flutter_test.dart';

class RollbackTransaction extends Error {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  var seedInTransaction = (var callback) async {
    var database = await DatabaseAdapter.instance.database;
    try {
      await database.transaction((txn) async {
        await txn.execute("DELETE FROM emotions");
        await EmotionRepository.seed(txn);
        await callback(txn);
      });
    } on RollbackTransaction catch (_) {}
  };

  group("EmotionRepository", () {
    group("#all", () {
      testWidgets("it finds the expected number of primary emotions",
          (WidgetTester _tester) async {
        _tester.takeException();
        await seedInTransaction((txn) async {
          var primaries =
              await EmotionRepository().all(dbToUse: txn, where: "depth = 0");
          expect(primaries.length, equals(6));
          throw new RollbackTransaction();
        });
      });

      testWidgets("it finds the expected number of secondary emotions",
          (WidgetTester _tester) async {
        _tester.takeException();
        await seedInTransaction((txn) async {
          var secondaries =
              await EmotionRepository().all(dbToUse: txn, where: "depth = 1");
          expect(secondaries.length, equals(26));
          throw new RollbackTransaction();
        });
      });

      testWidgets("it finds the expected number of tertiary emotions",
          (WidgetTester _tester) async {
        _tester.takeException();
        await seedInTransaction((txn) async {
          var tertiaries =
              await EmotionRepository().all(dbToUse: txn, where: "depth = 2");
          expect(tertiaries.length, equals(104));
          throw new RollbackTransaction();
        });
      });
    });

    group("#find", () {
      testWidgets("it returns a record that exists",
          (WidgetTester _tester) async {
        _tester.takeException();

        await seedInTransaction((txn) async {
          var emotion = await EmotionRepository().find(1, dbToUse: txn);
          expect(emotion, isNotNull);
          throw new RollbackTransaction();
        });
      });

      test("it returns null if the record does not exist", () async {
        var record = await EmotionRepository().find(999999);
        expect(record, isNull);
      });
    });
  });
}
