import 'package:emote/settings_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Displays app bar', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: SettingsRoute()));
    expect(find.text("Settings"), findsOneWidget);
  });

  testWidgets("Starts with notification schedule disabled",
      (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: SettingsRoute()));
    expect(find.text("At:"), findsNothing);
  });

  testWidgets("Enables notification schedule when ticked",
      (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(home: SettingsRoute()));
    await tester.tap(find.text("Reminders"));
    DropdownButtonFormField field =
        tester.widget(find.byType(DropdownButtonFormField));
    expect(field.enabled, isTrue);
  });
}
