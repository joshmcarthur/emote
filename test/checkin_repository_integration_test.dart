@Skip(
    "Tests requiring SQLite cannot run on machine, run on device with `flutter run`")

import 'package:emote/checkin_repository.dart';
import 'package:emote/database_adapter.dart';
import 'package:emote/emotion_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';

class RollbackTransaction extends Error {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  var insertCheckin = (var callback, {transaction}) async {
    var database = await DatabaseAdapter.instance.database;
    Checkin c = new Checkin();
    c.notes = "Lorem ipsum";

    var perform = (Transaction txn) async {
      c.emotionId = await txn.insert(Emotion.TABLE_NAME, {
        "label": DateTime.now().millisecondsSinceEpoch,
        "colour": "#ffffff",
        "depth": 0
      });

      await callback(await CheckinRepository().insert(c, dbToUse: txn));
    };

    try {
      await (transaction != null
          ? perform(transaction)
          : database.transaction(perform));
    } on RollbackTransaction catch (_) {}
  };

  group("CheckinRepository", () {
    group("#insert", () {
      testWidgets("it returns the persisted record ID",
          (WidgetTester _tester) async {
        _tester.takeException();
        var database = await DatabaseAdapter.instance.database;
        int count = (await database
            .rawQuery("SELECT COUNT(*) AS count FROM checkins;"))[0]["count"];
        await insertCheckin((recordId) async {
          expect(recordId, equals(count + 1));
          throw new RollbackTransaction();
        });
      });
    });

    group("#find", () {
      testWidgets("it returns a record that exists",
          (WidgetTester _tester) async {
        _tester.takeException();

        await (await DatabaseAdapter.instance.database)
            .transaction((txn) async {
          await insertCheckin((recordId) async {
            Checkin record =
                await CheckinRepository().find(recordId, dbToUse: txn);
            expectSync(record.id, equals(recordId));
            throw new RollbackTransaction();
          }, transaction: txn);
        });
      });

      test("it returns null if the record does not exist", () async {
        Checkin record = await CheckinRepository().find(999999);
        expect(record, isNull);
      });
    });

    group("#all", () {
      testWidgets("returns the expected number of records",
          (WidgetTester _tester) async {
        await (await DatabaseAdapter.instance.database)
            .transaction((txn) async {
          await txn.rawQuery("DELETE FROM checkins");
          await insertCheckin((recordId) async {
            List<Checkin> records = await CheckinRepository().all(dbToUse: txn);
            expectSync(records.length, equals(1));
            throw new RollbackTransaction();
          }, transaction: txn);
        });
      });
    });

    group("#public_checkin_data", () {
      testWidgets("returns the expected rows", (WidgetTester _tester) async {
        await (await DatabaseAdapter.instance.database)
            .transaction((txn) async {
          await txn.rawQuery("DELETE FROM checkins");
          await insertCheckin((recordId) async {
            Checkin record =
                await CheckinRepository().find(recordId, dbToUse: txn);
            Emotion emote =
                await EmotionRepository().find(record.emotionId, dbToUse: txn);
            List<Map<String, dynamic>> results =
                await CheckinRepository().publicCheckinData(dbToUse: txn);
            expect(results.length, 1);
            expect(results[0]["emotion"], emote.label);
            expect(results[0]["notes"], record.notes);
            expect(results[0].containsKey("_id"), false);
            expect(results[0]["created_at"].startsWith("20"), true);
            throw new RollbackTransaction();
          }, transaction: txn);
        });
      });
    });

    group("#group_by_date", () {
      testWidgets("returns the expected records", (WidgetTester _tester) async {
        await (await DatabaseAdapter.instance.database)
            .transaction((txn) async {
          await txn.rawQuery("DELETE FROM checkins");
          await insertCheckin((recordId) async {
            Map<String, List<Checkin>> grouped =
                await CheckinRepository().groupedByDate(dbToUse: txn);
            expectSync(grouped.keys,
                equals([DateFormat("dd-MM-y").format(DateTime.now())]));
            expectSync(grouped.values.toList()[0], hasLength(1));
            throw new RollbackTransaction();
          }, transaction: txn);
        });
      });
    });
  });
}
