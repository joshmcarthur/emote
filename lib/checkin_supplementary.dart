import 'package:emote/checkin_repository.dart';
import 'package:flutter/material.dart';

import 'emotion_repository.dart';
import 'home_route.dart';

class _CheckinSupplementaryRouteState extends State<CheckinSupplementaryRoute> {
  Emotion emotion;
  String notes;
  final _formKey = GlobalKey<FormState>();

  _CheckinSupplementaryRouteState(Emotion emotion) {
    this.emotion = emotion;
  }

  void performSubmit(context) async {
    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text("Adding to your timeline...")));
    var checkin =
        Checkin.fromMap({'emotion_id': this.emotion.id, 'notes': this.notes});
    await CheckinRepository().insert(checkin);
    // Back to the start
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => HomeRoute()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text("Anything to add?"),
        ),
        floatingActionButton: Builder(builder: (BuildContext context) {
          return FloatingActionButton.extended(
              key: Key("checkin_supplementary_submit"),
              icon: Icon(Icons.check, semanticLabel: "Submit"),
              onPressed: () => performSubmit(context),
              label: Text("Add to your timeline"));
        }),
        body: Form(
            key: _formKey,
            child: Padding(
                child: Column(children: [
                  TextFormField(
                      key: Key("checkin_supplementary_notes"),
                      autofocus: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText:
                              'What prompted feelings of ${this.emotion.label.toLowerCase()} today?',
                          helperText: 'Optional'),
                      keyboardType: TextInputType.multiline,
                      onChanged: (value) => setState(() {
                            notes = value;
                            emotion = this.emotion;
                          }),
                      maxLines: null),
                ]),
                padding: EdgeInsets.all(32))));
  }
}

class CheckinSupplementaryRoute extends StatefulWidget {
  final Emotion emotion;
  CheckinSupplementaryRoute({Key key, @required this.emotion})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CheckinSupplementaryRouteState(this.emotion);
  }
}
