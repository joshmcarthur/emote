import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationAdapter {
  static final notifications = new FlutterLocalNotificationsPlugin();
}
