import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'checkin_emotion.dart';
import 'home_route.dart';
import 'notification_adaptor.dart';

class EmoteApp extends StatelessWidget {
  static final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  EmoteApp() {
    var androidNotificationSettings =
        AndroidInitializationSettings('ic_launcher');
    var iosNotificationSettings = IOSInitializationSettings();
    WidgetsFlutterBinding.ensureInitialized();
    initializeDateFormatting('en', null);
    initializeDateFormatting('en_US,', null);

    NotificationAdapter.notifications.initialize(InitializationSettings(
        androidNotificationSettings, iosNotificationSettings));
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    NotificationAdapter.notifications
        .getNotificationAppLaunchDetails()
        .then((NotificationAppLaunchDetails launchDetails) {
      if (launchDetails.didNotificationLaunchApp) {
        Future.delayed(const Duration(milliseconds: 100), () async {
          await EmoteApp.navigatorKey.currentState.push(MaterialPageRoute(
              builder: (context) =>
                  CheckinEmotionRoute(resolver: DatabaseEmotionResolver())));
        });
      }
    });

    return MaterialApp(
      title: 'Emote',
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        bottomAppBarColor: Colors.deepPurple,
//        accentColor: Colors.black87,
      ),
      darkTheme: ThemeData.dark().copyWith(
        primaryColor: Colors.deepPurple,
        accentColor: Colors.white,
        bottomAppBarColor: Colors.deepPurple,
      ),
      home: HomeRoute(),
    );
  }
}
