import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import '../analytics_route.dart';
import '../checkin_repository.dart';
import '../emotion_repository.dart';

class AnalyticsTrendChartFourWeek extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AnalyticsTrendChartFourWeekState();
}

class AnalyticsTrendChartFourWeekState
    extends State<AnalyticsTrendChartFourWeek> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: chartData(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            throw (snapshot.error);
          }
          if (!snapshot.hasData) {
            return Container(child: Center(child: CircularProgressIndicator()));
          }

          return Container(
            child: Stack(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(
                      height: 56,
                      child: Center(
                          child: Text("Checkin trends (last 4 weeks)",
                              style: Theme.of(context).textTheme.bodyText2)),
                    ),
                    Expanded(
                      key: Key("analytics_route__four_week_trend_chart"),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                        child: LineChart(
                          snapshot.data,
                          swapAnimationDuration: Duration(milliseconds: 250),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  Future<LineChartData> chartData() async {
    var data = await aggregatedCheckinsByWeekNumber();
    return LineChartData(
        lineTouchData: LineTouchData(
          touchTooltipData: LineTouchTooltipData(
              fitInsideHorizontally: true,
              fitInsideVertically: true,
              tooltipBgColor: Theme.of(context).primaryColorDark,
              getTooltipItems: (spots) => spots.map((spot) {
                    String label =
                        "${(spot.bar.spots[spot.spotIndex] as EmotionFlSpot).emotion.label}: ${spot.y.round()} this week";
                    return LineTooltipItem(
                        label, Theme.of(context).primaryTextTheme.caption);
                  }).toList()),
          handleBuiltInTouches: true,
        ),
        gridData: const FlGridData(
          show: false,
        ),
        titlesData: FlTitlesData(
          bottomTitles: SideTitles(
            showTitles: true,
            reservedSize: 20,
            textStyle: Theme.of(context).textTheme.caption,
            margin: 16,
            getTitles: (value) {
              const labels = {
                0: 'This week',
                1: 'Last week',
                2: '2w ago',
                3: '3w ago',
                4: '4w ago'
              };

              return labels.containsKey(value) ? labels[value] : '';
            },
          ),
          leftTitles: SideTitles(
            showTitles: true,
            textStyle: Theme.of(context).textTheme.caption,
            getTitles: (value) => value.toInt().toString(),
            // This should always be the count
            margin: 20,
            reservedSize: 16,
          ),
        ),
        borderData: FlBorderData(
          show: true,
          border: Border(
            bottom: BorderSide(
              color: Theme.of(context).textTheme.caption.color,
              width: 2,
            ),
            left: BorderSide(
              color: Theme.of(context).textTheme.caption.color,
              width: 2,
            ),
            right: BorderSide(
              color: Colors.transparent,
            ),
            top: BorderSide(
              color: Colors.transparent,
            ),
          ),
        ),
        minX: -1,
        maxX: 5,
        maxY: [
          3.0,
          data.length == 0
              ? 0
              : data
                  .map((bar) => bar.spots.map((spot) => spot.y))
                  .expand((item) => item)
                  .reduce(max)
        ].reduce(max),
        minY: 0,
        lineBarsData: data);
  }

  Future<List<LineChartBarData>> aggregatedCheckinsByWeekNumber() async {
    var roots = await EmotionRepository().all(where: "parent_id IS NULL");
    var chartDataList = await Future.wait(roots.map((Emotion emotion) =>
        aggregatedCheckinsByWeekNumberAndEmotion(emotion)));

    return chartDataList.where((d) => d != null).toList();
  }

  Future<LineChartBarData> aggregatedCheckinsByWeekNumberAndEmotion(
      Emotion emotion) async {
    var db = await CheckinRepository().defaultDatabase();
    var weeklyCheckins = await db.rawQuery("""
        SELECT
          cast(strftime('%W', CURRENT_TIMESTAMP) as real) - cast(strftime('%W', created_at) as real) AS weeks_ago,
          cast(count(*) as real) AS checkin_count
        FROM checkins
        WHERE 
          weeks_ago >= 0 AND 
          weeks_ago <= 4 AND
          emotion_id IN (
            WITH RECURSIVE emotion_tree(id, parent_id) as (
                SELECT 
                  _id, _id 
                FROM emotions 
                WHERE 
                  parent_id is null
        
                UNION ALL
        
                SELECT 
                  emotions._id, emotion_tree.parent_id
                FROM emotions, emotion_tree
                WHERE 
                  emotions.parent_id = emotion_tree.id
            )
            SELECT 
              id 
            FROM 
              emotion_tree 
            WHERE 
              parent_id = ${emotion.id}
          )
          GROUP BY weeks_ago
      """);

    var compactedWeeklyCheckins =
        weeklyCheckins.where((row) => row["weeks_ago"] != null).toList();
    if (compactedWeeklyCheckins.isEmpty) {
      return null;
    }

    return Future.value(LineChartBarData(
      spots: compactedWeeklyCheckins
          .map((row) =>
              EmotionFlSpot(row["weeks_ago"], row["checkin_count"], emotion))
          .toList(),
      isCurved: true,
      colors: [emotion.tint()],
      barWidth: 4,
      isStrokeCapRound: true,
      dotData: FlDotData(
          show: true, dotColor: Theme.of(context).primaryColor, dotSize: 8),
      belowBarData: BarAreaData(
        show: false,
      ),
    ));
  }
}
