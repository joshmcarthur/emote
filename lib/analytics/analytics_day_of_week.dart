import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import '../checkin_repository.dart';
import '../emotion_repository.dart';

class EmotionBarChartStackItem extends BarChartRodStackItem {
  final Emotion emotion;
  EmotionBarChartStackItem(double fromY, double toY, this.emotion)
      : super(fromY, toY, emotion.tint());
}

class AnalyticsTrendChartDayOfWeek extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AnalyticsTrendChartDayOfWeekState();
}

class AnalyticsTrendChartDayOfWeekState
    extends State<AnalyticsTrendChartDayOfWeek> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: chartData(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            throw (snapshot.error);
          }
          if (!snapshot.hasData) {
            return Container(child: Center(child: CircularProgressIndicator()));
          }

          return Container(
            child: Stack(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(
                      height: 56,
                      child: Center(
                          child: Text("Checkins by day of week",
                              style: Theme.of(context).textTheme.bodyText2)),
                    ),
                    Expanded(
                      key: Key(
                          "analytics__chart_analytics_trend_chart_day_of_week"),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                        child: BarChart(
                          snapshot.data,
                          swapAnimationDuration: Duration(milliseconds: 250),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  Future<BarChartData> chartData() async {
    var data = await aggregatedCheckinsByDayOfWeek();
    return BarChartData(
        alignment: BarChartAlignment.spaceEvenly,
        barTouchData: BarTouchData(
            touchTooltipData: BarTouchTooltipData(
                fitInsideHorizontally: true,
                fitInsideVertically: true,
                tooltipBgColor: Theme.of(context).primaryColorDark,
                getTooltipItem: (group, groupIndex, rod, rodIndex) {
                  var label = rod.rodStackItem
                      .map((item) =>
                          "${(item as EmotionBarChartStackItem).emotion.label}: ${(item.toY - item.fromY).round()}")
                      .join("\n");
                  return BarTooltipItem(
                      label, Theme.of(context).primaryTextTheme.caption);
                })),
        gridData: const FlGridData(
          show: false,
        ),
        titlesData: FlTitlesData(
          bottomTitles: SideTitles(
            showTitles: true,
            reservedSize: 20,
            textStyle: Theme.of(context).textTheme.caption,
            margin: 16,
            getTitles: (value) {
              const labels = {
                0: 'Mon',
                1: 'Tue',
                2: 'Wed',
                3: 'Thu',
                4: 'Fri',
                5: 'Sat',
                6: 'Sun'
              };

              return labels.containsKey(value) ? labels[value] : '';
            },
          ),
          leftTitles: SideTitles(
            showTitles: true,
            textStyle: Theme.of(context).textTheme.caption,
            getTitles: (value) => value.toInt().toString(),
            // This should always be the count
            margin: 20,
            reservedSize: 16,
          ),
        ),
        borderData: FlBorderData(
          show: true,
          border: Border(
            bottom: BorderSide(
              color: Theme.of(context).textTheme.caption.color,
              width: 2,
            ),
            left: BorderSide(
              color: Theme.of(context).textTheme.caption.color,
              width: 2,
            ),
            right: BorderSide(
              color: Colors.transparent,
            ),
            top: BorderSide(
              color: Colors.transparent,
            ),
          ),
        ),
        maxY: [
          3.0,
          data
              .map((bar) => bar.barRods.map((rod) => rod.y))
              .expand((item) => item)
              .reduce(max)
        ].reduce(max),
        barGroups: data);
  }

  Future<List<BarChartGroupData>> aggregatedCheckinsByDayOfWeek() async {
    var roots = await EmotionRepository().all(where: "parent_id IS NULL");
    List<BarChartGroupData> groups = [];
    for (var i = 0; i < 7; i++) {
      var ySum = 0.0;
      var stackList = await Future.wait(roots.map((Emotion emotion) async {
        var result = await aggregatedCheckinsByDayOfWeekAndEmotion(i, emotion);
        if (result == null) return null;
        return EmotionBarChartStackItem(
            ySum.toDouble(), ySum += result["checkin_count"], emotion);
      }));

      groups.add(BarChartGroupData(x: i, barsSpace: 8, barRods: [
        BarChartRodData(
            y: ySum,
            width: 32,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(4), topRight: Radius.circular(4)),
            rodStackItem: stackList.where((d) => d != null).toList())
      ]));
    }

    return Future.value(groups);
  }

  Future<Map<String, dynamic>> aggregatedCheckinsByDayOfWeekAndEmotion(
      int dayOfWeek, Emotion emotion) async {
    var db = await CheckinRepository().defaultDatabase();
    var dayOfWeekCheckins = await db.rawQuery("""
        SELECT
          cast(strftime('%w', created_at) as real) AS day_of_week,
          cast(count(*) as real) AS checkin_count
        FROM checkins
        WHERE 
          day_of_week = $dayOfWeek
          AND 
          emotion_id IN (
            WITH RECURSIVE emotion_tree(id, parent_id) as (
                SELECT 
                  _id, _id 
                FROM emotions 
                WHERE 
                  parent_id is null
        
                UNION ALL
        
                SELECT 
                  emotions._id, emotion_tree.parent_id
                FROM emotions, emotion_tree
                WHERE 
                  emotions.parent_id = emotion_tree.id
            )
            SELECT 
              id 
            FROM 
              emotion_tree 
            WHERE 
              parent_id = ${emotion.id}
            OR
              id = ${emotion.id}
          )
          LIMIT 1;
      """);

    var compactedDayOfWeekCheckins =
        dayOfWeekCheckins.where((row) => row["day_of_week"] != null).toList();
    if (compactedDayOfWeekCheckins.isEmpty) {
      return null;
    }

    return compactedDayOfWeekCheckins[0];
  }
}
