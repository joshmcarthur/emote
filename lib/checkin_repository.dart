// singleton class to manage the database
import 'package:collection/collection.dart';
import 'package:sqflite/sqflite.dart';

import 'database_adapter.dart';
import 'emotion_repository.dart';

class Checkin {
  int id;
  int emotionId;
  String notes;
  DateTime createdAt;

  static const String TABLE_NAME = "checkins";
  static const String COLUMN_ID = "_id";
  static const String COLUMN_EMOTION_ID = "emotion_id";
  static const String COLUMN_NOTES = "notes";
  static const String COLUMN_CREATED_AT = "created_at";

  Checkin({this.id, this.emotionId, this.notes});

  Future<Emotion> emotion() {
    return EmotionRepository().find(emotionId);
  }

  Checkin.fromMap(Map<String, dynamic> map) {
    id = map[COLUMN_ID];
    notes = map[COLUMN_NOTES];
    emotionId = map[COLUMN_EMOTION_ID];

    if (map[COLUMN_CREATED_AT] != null) {
      createdAt = DateTime.parse(map[COLUMN_CREATED_AT] + "Z");
    }
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      COLUMN_ID: id,
      COLUMN_EMOTION_ID: emotionId,
      COLUMN_NOTES: notes,
    };
  }
}

class CheckinRepository {
  Future<Database> defaultDatabase() {
    return DatabaseAdapter.instance.database;
  }

  Future<int> insert(Checkin checkin, {var dbToUse}) async {
    var db = dbToUse ?? await defaultDatabase();
    int id = await db.insert(Checkin.TABLE_NAME, checkin.toMap());
    return id;
  }

  Future<Checkin> find(int id, {var dbToUse}) async {
    var db = dbToUse ?? await defaultDatabase();
    List<Map> maps = await db.query(Checkin.TABLE_NAME,
        columns: [
          Checkin.COLUMN_ID,
          Checkin.COLUMN_EMOTION_ID,
          Checkin.COLUMN_NOTES,
          Checkin.COLUMN_CREATED_AT
        ],
        where: '${Checkin.COLUMN_ID} = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return Checkin.fromMap(maps.first);
    }
    return null;
  }

  Future<List<Checkin>> all({var dbToUse}) async {
    var db = dbToUse ?? await defaultDatabase();
    List<Map> maps = await db.query(Checkin.TABLE_NAME);

    return List.from(maps.map((data) => new Checkin.fromMap(data)));
  }

  Future<Map<String, List<Checkin>>> groupedByDate({var dbToUse}) async {
    var db = dbToUse ?? await defaultDatabase();
    const String dateFormat = "%d-%m-%Y";
    final String groupingQuery = """
      SELECT DISTINCT * FROM ${Checkin.TABLE_NAME} INNER JOIN (
         SELECT strftime("$dateFormat", datetime(created_at, 'localtime')) AS local_created_date
         FROM ${Checkin.TABLE_NAME}
        GROUP BY created_at
      )
      ON strftime("$dateFormat", datetime(created_at, 'localtime'))=local_created_date
      ORDER BY created_at DESC;
    """;

    List<Map> rawGroupedByDate = await db.rawQuery(groupingQuery);
//    print(rawGroupedByDate);
//    print(groupBy(rawGroupedByDate, (result) => result["created_date"]));
    Map<String, List<Checkin>> result = groupBy(
            rawGroupedByDate, (result) => result["local_created_date"])
        .map((date, checkins) =>
            MapEntry(date, checkins.map((ci) => Checkin.fromMap(ci)).toList()));
    return result;
  }

  Future<List<Map<String, dynamic>>> publicCheckinData({var dbToUse}) async {
    var db = dbToUse ?? await defaultDatabase();
    final exportQuery = """
      SELECT created_at, emotions.label AS emotion, notes
      FROM checkins
      INNER JOIN emotions ON emotions._id = checkins.emotion_id
      ORDER BY created_at asc;
    """;

    return db.rawQuery(exportQuery);
  }
}
