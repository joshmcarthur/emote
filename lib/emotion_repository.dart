import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

import 'database_adapter.dart';

class Emotion {
  int id;
  String label;
  String colour;
  int depth;
  int parentId;

  static const String TABLE_NAME = "emotions";
  static const String COLUMN_ID = "_id";
  static const String COLUMN_LABEL = "label";
  static const String COLUMN_COLOUR = "colour";
  static const String COLUMN_DEPTH = "depth";
  static const String COLUMN_PARENT_ID = "parent_id";

  Emotion(this.label, this.colour, [this.depth = 0, this.parentId]);

  Color tint() => (Color(int.parse(colour.replaceFirst('#', ''), radix: 16))
      .withAlpha((255 / (depth == 0 ? 1 : depth)).floor()));

  Emotion.fromMap(Map<String, dynamic> map) {
    id = map[COLUMN_ID];
    label = map[COLUMN_LABEL];
    depth = map[COLUMN_DEPTH];
    colour = map[COLUMN_COLOUR];
    parentId = map[COLUMN_PARENT_ID];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      COLUMN_ID: id,
      COLUMN_LABEL: label,
      COLUMN_DEPTH: depth,
      COLUMN_COLOUR: colour,
      COLUMN_PARENT_ID: parentId
    };
  }
}

final Map<String, String> defaultPalette = {
  "Love": "#d50000",
  "Joy": "#00c853",
  "Surprise": "#0091ea",
  "Anger": "#dd2c00",
  "Sadness": "#37474f",
  "Fear": "#ffab00"
};

final Map<String, Map<String, List<String>>> emotionTree = {
  "Love": {
    "Affection": [
      "Adoration",
      "Fondness",
      "Liking",
      "Attraction",
      "Caring",
      "Tenderness",
      "Compassion",
      "Sentimentality",
    ],
    "Lust/Sexual desire": [
      "Desire",
      "Passion",
      "Infatuation",
    ],
    "Longing": [],
  },
  "Joy": {
    "Cheerfulness": [
      "Amusement",
      "Bliss",
      "Gaiety",
      "Glee",
      "Jolliness",
      "Joviality",
      "Delight",
      "Enjoyment",
      "Gladness",
      "Happiness",
      "Jubilation",
      "Elation",
      "Satisfaction",
      "Ecstasy",
      "Euphoria",
    ],
    "Zest": [
      "Enthusiasm",
      "Zeal",
      "Excitement",
      "Thrill",
      "Exhilaration",
    ],
    "Contentment": [
      "Pleasure",
    ],
    "Pride": [
      "Triumph",
    ],
    "Optimism": [
      "Eagerness",
      "Hope",
    ],
    "Enthrallment": [
      "Rapture",
    ],
    "Relief": []
  },
  "Surprise": {"Amazement": [], "Astonishment": []},
  "Anger": {
    "Irritability": [
      "Aggravation",
      "Agitation",
      "Annoyance",
      "Grouchy",
      "Grumpy",
      "Crosspatch",
    ],
    "Exasperation": ["Frustration"],
    "Rage": [
      "Outrage",
      "Fury",
      "Wrath",
      "Hostility",
      "Ferocity",
      "Bitterness",
      "Hatred",
      "Scorn",
      "Spite",
      "Vengefulness",
      "Dislike",
      "Resentment",
    ],
    "Disgust": [
      "Revulsion",
      "Contempt",
      "Loathing",
    ],
    "Envy": ["Jealousy"],
    "Torment": []
  },
  "Sadness": {
    "Suffering": [
      "Agony",
      "Anguish",
      "Hurt",
    ],
    "Sadness ": [
      "Depression",
      "Despair",
      "Gloom",
      "Glumness",
      "Unhappiness",
      "Grief",
      "Sorrow",
      "Woe",
      "Misery",
      "Melancholy",
    ],
    "Disappointment": [
      "Dismay",
      "Displeasure",
    ],
    "Shame": [
      "Guilt",
      "Regret",
      "Remorse",
    ],
    "Neglect": [
      "Alienation",
      "Defeatism",
      "Dejection",
      "Embarrassment",
      "Homesickness",
      "Humiliation",
      "Insecurity",
      "Insult",
      "Isolation",
      "Loneliness",
      "Rejection",
    ],
    "Sympathy": [
      "Pity",
      "Mono no aware",
    ]
  },
  "Fear": {
    "Horror": [
      "Alarm",
      "Shock",
      "Fright",
      "Terror",
      "Panic",
      "Hysteria",
      "Mortification",
    ],
    "Nervousness": [
      "Anxiety",
      "Suspense",
      "Uneasiness",
      "Apprehension (fear)",
      "Worry",
      "Distress",
      "Dread",
    ]
  }
};

class EmotionRepository {
  Future<Database> defaultDatabase() {
    return DatabaseAdapter.instance.database;
  }

  Future<List<Emotion>> all({dbToUse, where, whereArgs}) async {
    DatabaseExecutor db = dbToUse ?? await defaultDatabase();
    List<Map> maps = [];
    if (where != null && whereArgs != null) {
      maps = await db.query(Emotion.TABLE_NAME,
          where: where, whereArgs: whereArgs);
    } else if (where != null) {
      maps = await db.query(Emotion.TABLE_NAME, where: where);
    } else {
      maps = await db.query(Emotion.TABLE_NAME);
    }

    return List.from(maps.map((data) => new Emotion.fromMap(data)));
  }

  Future<Emotion> find(int id, {var dbToUse}) async {
    List<Emotion> maps = await all(
        dbToUse: dbToUse, where: '${Emotion.COLUMN_ID} = ?', whereArgs: [id]);
    if (maps.length > 0) {
      return maps.first;
    }
    return null;
  }

  static seed(DatabaseExecutor db) async {
    var insert = (label, palette, depth, parentId) => db.insert(
        Emotion.TABLE_NAME, Emotion(label, palette, depth, parentId).toMap());

    return Future.wait(emotionTree.entries.map((entry) {
      var palette = defaultPalette[entry.key];
      return insert(entry.key, palette, 0, null).then((parentId) =>
          (Future.wait(entry.value.entries.map((entry) =>
              (insert(entry.key, palette, 1, parentId).then((parentId) =>
                  (Future.wait(entry.value.map(
                      (label) => (insert(label, palette, 2, parentId)))))))))));
    }));
  }
}
