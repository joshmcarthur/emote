import 'dart:async';
import 'dart:io';

import 'package:emote/emotion_repository.dart';
import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'checkin_repository.dart';

class DatabaseAdapter {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "emote.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 3;

  // Make this a singleton class.
  DatabaseAdapter._privateConstructor();
  static final DatabaseAdapter instance = DatabaseAdapter._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);

    // Uncomment this to load a fixture DB
    // return await openTestDatabase();

    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  Future openTestDatabase() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "emote.seeded.db");

    // delete existing if any
    await deleteDatabase(path);

    // Make sure the parent directory exists
    try {
      await Directory(dirname(path)).create(recursive: true);
    } catch (_) {}

    // Copy from asset
    ByteData data = await rootBundle.load(join("assets", "emote.seeded.db"));
    List<int> bytes =
        data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    await new File(path).writeAsBytes(bytes, flush: true);

    // open the database
    return await openDatabase(path);
  }

  Future _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < 3 && newVersion == 3) {
      await db.transaction((txn) async {
        await txn.execute("ALTER TABLE checkins RENAME TO old_checkins");
        await _onCreate(txn, 0);

        await txn.execute("""
          INSERT INTO checkins(_id, created_at, notes, emotion_id)
          SELECT old_checkins._id, old_checkins.created_at, notes, emotions._id 
          FROM old_checkins
          INNER JOIN emotions ON emotions.label=old_checkins.emotion;
        """);

        await txn.execute("DROP TABLE old_checkins");
      }, exclusive: true);
    }
  }

  // SQL string to create the database
  Future _onCreate(DatabaseExecutor db, int version) async {
    await db.execute('''
              CREATE TABLE ${Emotion.TABLE_NAME} (
                ${Emotion.COLUMN_ID} INTEGER PRIMARY KEY,
                ${Emotion.COLUMN_LABEL} TEXT NOT NULL UNIQUE,
                ${Emotion.COLUMN_COLOUR} TEXT NOT NULL,
                ${Emotion.COLUMN_DEPTH} INTEGER DEFAULT 0,
                ${Emotion.COLUMN_PARENT_ID} INTEGER REFERENCES emotions(_id)
              );
    ''');

    await db.execute('''
              CREATE TABLE ${Checkin.TABLE_NAME} (
                ${Checkin.COLUMN_ID} INTEGER PRIMARY KEY,
                ${Checkin.COLUMN_EMOTION_ID} INTEGER NOT NULL REFERENCES emotions(_id),
                ${Checkin.COLUMN_NOTES} TEXT,
                ${Checkin.COLUMN_CREATED_AT} TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
              );
    ''');

    await EmotionRepository.seed(db);
  }
}
