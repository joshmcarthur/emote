import 'package:emote/checkin_repository.dart';
import 'package:emote/notification_adaptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:csv/csv.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';

/// Convert a map list to csv
String _mapListToCsv(List<Map<String, dynamic>> mapList,
    {ListToCsvConverter converter}) {
  if (mapList == null) {
    return null;
  }
  converter ??= const ListToCsvConverter();
  var data = <List>[];
  var keys = <String>[];
  var keyIndexMap = <String, int>{};

  // Add the key and fix previous records
  int _addKey(String key) {
    var index = keys.length;
    keyIndexMap[key] = index;
    keys.add(key);
    for (var dataRow in data) {
      dataRow.add(null);
    }
    return index;
  }

  for (var map in mapList) {
    // This list might grow if a new key is found
    var dataRow = List(keyIndexMap.length);
    // Fix missing key
    map.forEach((key, value) {
      var keyIndex = keyIndexMap[key];
      if (keyIndex == null) {
        // New key is found
        // Add it and fix previous data
        keyIndex = _addKey(key);
        // grow our list
        dataRow = List.from(dataRow, growable: true)..add(value);
      } else {
        dataRow[keyIndex] = value;
      }
    });
    data.add(dataRow);
  }
  return converter.convert(<List>[]
    ..add(keys)
    ..addAll(data));
}

class SettingsState extends State<SettingsRoute> {
  static const String _REMINDER_FREQUENCY_PREF_KEY =
      "preference__reminder_frequency";
  static const String _REMINDER_DAY_PREF_KEY = "preference__reminder_day";
  static const String _HAS_REMINDERS_PREF_KEY = "preference__has_reminders";

  bool hasReminders = false;
  bool isExporting = false;
  String reminderFrequency = "Daily";
  var reminderTimes = <String, TimeOfDay>{};
  var reminderDay;

  @override
  initState() {
    super.initState();
    loadPreferences();
  }

  static final _androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      "com.joshmcarthur.emote.checkin_reminders",
      "Emote Checkin Reminders",
      "Reminders will pop up a notification on your device to remind you to check in. Manage notification schedule in-app.",
      importance: Importance.Default,
      priority: Priority.Default,
      ticker: 'Checkin Reminder');

  static final _iosPlatformChannelSpecifics = new IOSNotificationDetails();
  static final _notificationTitle = "It's time to check in.";
  var channelSpecifics = new NotificationDetails(
      _androidPlatformChannelSpecifics, _iosPlatformChannelSpecifics);

  setPreference(String key, value) async {
    print("Setting $key to $value");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (value is bool)
      prefs.setBool(key, value);
    else if (value is String)
      prefs.setString(key, value);
    else if (value is int) prefs.setInt(key, value);
  }

  loadPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      reminderFrequency =
          prefs.getString(_REMINDER_FREQUENCY_PREF_KEY) ?? "Daily";
      reminderDay = prefs.getString(_REMINDER_DAY_PREF_KEY) ?? null;
      hasReminders = prefs.getBool(_HAS_REMINDERS_PREF_KEY) ?? false;

      ["reminder1", "reminder2", "reminder3"].forEach((prefKey) {
        var timeParts =
            prefs.getString("preference__${prefKey}_time")?.split(":");
        if (timeParts != null && timeParts.length == 2) {
          reminderTimes[prefKey] = TimeOfDay(
              hour: int.parse(timeParts[0]), minute: int.parse(timeParts[1]));
        }
      });
    });
  }

  clearSchedule() async {
    var notifications =
        await NotificationAdapter.notifications.pendingNotificationRequests();
    print("Clearing ${notifications.length} pending notifications...");
    notifications.forEach((notification) =>
        NotificationAdapter.notifications.cancel(notification.id));
  }

  scheduleNotifications() async {
    var notifications = NotificationAdapter.notifications;

    scheduleDaily(time, {id = ""}) {
      print("Scheduling daily notification for ${time.toString()}");
      notifications.showDailyAtTime(
          "${_androidPlatformChannelSpecifics.channelId}_$id".hashCode,
          _notificationTitle,
          null,
          Time(time.hour, time.minute),
          channelSpecifics);
    }

    await clearSchedule();

    switch (reminderFrequency) {
      case "Daily":
        if (reminderTimes['reminder1'] != null)
          scheduleDaily(reminderTimes['reminder1'], id: "reminder1");
        break;
      case "Twice per day":
        List.generate(2, (idx) {
          if (reminderTimes["reminder${idx + 1}"] != null) {
            var key = "reminder${idx + 1}";
            scheduleDaily(reminderTimes[key], id: key);
          }
        });
        break;
      case "Three times a day":
        List.generate(3, (idx) {
          if (reminderTimes["reminder${idx + 1}"] != null) {
            var key = "reminder${idx + 1}";
            scheduleDaily(reminderTimes[key], id: key);
          }
        });
        break;
      case "Weekly":
        var time = reminderTimes['reminder1'];
        notifications.showWeeklyAtDayAndTime(
            "${_androidPlatformChannelSpecifics.channelId}_reminder_weekly"
                .hashCode,
            _notificationTitle,
            null,
            Day(reminderDay),
            Time(time.hour, time.minute),
            channelSpecifics);
        break;
    }
  }

  ListTile generateTimeField(stateName) => ListTile(
      title: Text("At"),
      trailing: Text(reminderTimes[stateName]?.format(context) ?? "--:-- AM"),
      onTap: () async {
        var time = await showTimePicker(
            context: context, initialTime: TimeOfDay.now());
        setState(() {
          reminderTimes[stateName] = time;
          setPreference(
              "preference__${stateName}_time", '${time.hour}:${time.minute}');
          scheduleNotifications();
        });
      });

  @override
  Widget build(BuildContext context) {
    final exportListTile = ListTile(
        title: Text(isExporting ? "Exporting..." : "Export"),
        enabled: !isExporting,
        subtitle:
            Text(isExporting ? "" : "Export your checkin data as a CSV file."),
        onTap: () async {
          setState(() => isExporting = true);
          List<Map<String, dynamic>> exportData =
              await CheckinRepository().publicCheckinData();
          String csv = _mapListToCsv(exportData);
          await Share.file(
              'Emote checkin export',
              'emote-${DateTime.now().toIso8601String()}.csv',
              csv.codeUnits,
              'text/csv');
          setState(() => isExporting = false);
        });

    var extraFields = <Widget>[];
    switch (reminderFrequency) {
      case "Daily":
        extraFields = [generateTimeField("reminder1")];
        break;
      case "Weekly":
        extraFields = [
          ListTile(
              title: Text("On"),
              trailing: DropdownButton(
                  value: reminderDay,
                  onChanged: (value) => setState(() {
                        setPreference(_REMINDER_DAY_PREF_KEY, int.parse(value));
                        reminderDay = int.parse(value);
                      }),
                  items: [
                    [Day.Monday, 'Monday'],
                    [Day.Tuesday, 'Tuesday'],
                    [Day.Wednesday, 'Wednesday'],
                    [Day.Thursday, 'Thursday'],
                    [Day.Friday, 'Friday'],
                    [Day.Saturday, 'Saturday'],
                    [Day.Sunday, 'Sunday']
                  ].map<DropdownMenuItem<String>>((data) {
                    return DropdownMenuItem<String>(
                        value: (data[0] as Day).value.toString(),
                        child: Text(data[1]));
                  }).toList())),
          generateTimeField("reminder1")
        ];
        break;
      case "Twice per day":
        extraFields = [
          generateTimeField("reminder1"),
          generateTimeField("reminder2")
        ];
        break;
      case "Three times a day":
        extraFields = [
          generateTimeField("reminder1"),
          generateTimeField("reminder2"),
          generateTimeField("reminder3")
        ];
        break;
    }

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Settings"),
      ),
      body: ListView(
        children: <Widget>[
              SwitchListTile(
                title: const Text('Reminders'),
                isThreeLine: true,
                subtitle: const Text(
                    'Reminders will pop up a notification on your device to remind you to check in.'),
                value: hasReminders,
                onChanged: (bool value) {
                  setState(() {
                    hasReminders = value;
                    setPreference(_HAS_REMINDERS_PREF_KEY, value);
                    if (!hasReminders) clearSchedule();
                  });
                },
              ),
              Padding(
                  child: DropdownButtonFormField(
                      disabledHint: Text(reminderFrequency),
                      value: reminderFrequency,
                      onChanged: (value) => setState(() {
                            setPreference(_REMINDER_FREQUENCY_PREF_KEY, value);
                            reminderFrequency = value;
                          }),
                      items: !hasReminders
                          ? []
                          : <String>[
                              'Weekly',
                              'Daily',
                              'Twice per day',
                              'Three times a day'
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                  value: value, child: Text(value));
                            }).toList()),
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16)),
            ] +
            (hasReminders ? extraFields : []) +
            [Divider(), exportListTile, AboutListTile()],
      ),
    );
  }
}

class SettingsRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SettingsState();
  }
}
