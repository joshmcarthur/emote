import 'package:emote/checkin_supplementary.dart';
import 'package:emote/emotion_repository.dart';
import 'package:flutter/material.dart';

abstract class EmotionResolver {
  Future<List<Emotion>> root();
  Future<List<Emotion>> childrenOf(int parentId);
}

class DatabaseEmotionResolver extends EmotionResolver {
  Future<List<Emotion>> root() {
    return EmotionRepository()
        .all(where: "${Emotion.COLUMN_PARENT_ID} IS NULL");
  }

  Future<List<Emotion>> childrenOf(int parentId) {
    return EmotionRepository()
        .all(where: "${Emotion.COLUMN_PARENT_ID} = ?", whereArgs: [parentId]);
  }
}

class EmotionChoiceChip extends StatelessWidget {
  EmotionChoiceChip(
      {Key key, this.emotion, this.onSelected, this.isSelected = false})
      : super(key: key);

  final Emotion emotion;
  final Function(Emotion, bool) onSelected;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;

    return ChoiceChip(
        label: Text(emotion.label),
        avatar: CircleAvatar(backgroundColor: emotion.tint()),
        selectedColor: Theme.of(context).primaryColorLight,
        selected: isSelected,
        labelStyle: shortestSide < 600
            ? Theme.of(context).textTheme.button
            : Theme.of(context).textTheme.headline5,
        onSelected: (bool selected) => onSelected(emotion, selected));
  }
}

class CheckinEmotionRoute extends StatefulWidget {
  final EmotionResolver resolver;
  CheckinEmotionRoute({this.resolver});

  @override
  State<StatefulWidget> createState() {
    return new CheckinEmotionState(this.resolver);
  }
}

class CheckinEmotionState extends State<CheckinEmotionRoute> {
  Emotion selectedPrimary;
  Emotion selectedSecondary;
  Emotion selectedTertiary;
  EmotionResolver resolver;

  List<Emotion> primaries = [];
  List<Emotion> secondaries = [];
  List<Emotion> tertiaries = [];

  CheckinEmotionState(this.resolver);

  List<Widget> toChips(Iterable options, Function(Emotion, bool) wasSelected) {
    return options
        .map<Widget>((emotion) => (EmotionChoiceChip(
            emotion: emotion,
            onSelected: wasSelected,
            isSelected: selectedPrimary == emotion ||
                selectedSecondary == emotion ||
                selectedTertiary == emotion)))
        .toList();
  }

  doSubmit() {
    if ((selectedTertiary ?? selectedSecondary ?? selectedPrimary) != null) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CheckinSupplementaryRoute(
                  emotion: selectedTertiary ??
                      selectedSecondary ??
                      selectedPrimary)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text("How are you feeling?")),
        floatingActionButton: FloatingActionButton.extended(
            key: Key("checkin_emotion_submit"),
            icon: Icon(Icons.arrow_forward, semanticLabel: "Continue"),
            onPressed: doSubmit,
            label: Text("Continue")),
        body: FutureBuilder(
            future: resolver.root(),
            builder: (context, AsyncSnapshot<List<Emotion>> snapshot) {
              if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              }
              if (!snapshot.hasData) {
                return new Center(child: new CircularProgressIndicator());
              }

              primaries = snapshot.data;
              bool notNull(Object o) => o != null;

              return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SingleChildScrollView(
                        child: Padding(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Wrap(
                                      alignment: WrapAlignment.center,
                                      spacing: 8.0,
                                      runSpacing: 8.0,
                                      children: toChips(
                                          primaries, handleChipSelection)),
                                  (secondaries.length > 0
                                      ? Padding(
                                          child: Divider(),
                                          padding: EdgeInsets.only(
                                              top: 16, bottom: 16))
                                      : null),
                                  Wrap(
                                      spacing: 8.0,
                                      runSpacing: 8.0,
                                      alignment: WrapAlignment.center,
                                      children: toChips(
                                          secondaries, handleChipSelection)),
                                  (tertiaries.length > 0
                                      ? Padding(
                                          child: Divider(),
                                          padding: EdgeInsets.only(
                                              top: 8, bottom: 8))
                                      : null),
                                  Wrap(
                                      spacing: 8.0,
                                      runSpacing: 8.0,
                                      alignment: WrapAlignment.center,
                                      children: toChips(
                                          tertiaries, handleChipSelection)),
                                ].where(notNull).toList()),
                            padding: new EdgeInsets.all(32)))
                  ]);
            }));
  }

  handleChipSelection(Emotion emotion, bool wasSelected) async {
    if (emotion.depth == 0) {
      selectedPrimary = emotion;
      selectedSecondary = null;
      selectedTertiary = null;
      secondaries = await resolver.childrenOf(emotion.id);
      tertiaries.clear();
    } else if (emotion.depth == 1) {
      selectedSecondary = emotion;
      selectedTertiary = null;
      tertiaries = await resolver.childrenOf(emotion.id);
      if (tertiaries == null || tertiaries.isEmpty) {
        tertiaries = [];
        doSubmit();
      }
    } else if (emotion.depth == 2) {
      selectedTertiary = emotion;
      doSubmit();
    }

    setState(() {
      selectedPrimary = selectedPrimary;
      selectedSecondary = selectedSecondary;
      selectedTertiary = selectedTertiary;
      tertiaries = tertiaries;
      secondaries = secondaries;
    });
  }
}
