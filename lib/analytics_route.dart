import 'package:emote/analytics/analytics_day_of_week.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import 'analytics/analytics_four_week_trend_chart.dart';
import 'analytics/analytics_by_month_trend_chart.dart';
import 'emotion_repository.dart';

class EmotionFlSpot extends FlSpot {
  final Emotion emotion;

  EmotionFlSpot(double x, double y, this.emotion) : super(x, y);
}

class AnalyticsRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AnalyticsState();
}

enum AnalyticsChartMode {
  last_4_weeks,
  last_6_months,
  last_12_months,
  day_of_week
}

class AnalyticsState extends State<AnalyticsRoute> {
  AnalyticsChartMode viewMode;

  @override
  void initState() {
    super.initState();
    viewMode = AnalyticsChartMode.last_4_weeks;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        initialIndex: 0,
        child: Scaffold(
            extendBodyBehindAppBar: false,
            appBar: AppBar(
                bottom: TabBar(
                  tabs: [
                    Tab(
                        text: "4 weeks",
                        key: Key(
                            "analytics__tab_analytics_trend_chart_four_week")),
                    Tab(
                        text: "6 months",
                        key: Key(
                            "analytics__tab_analytics_trend_chart_by_month_6")),
                    Tab(
                        text: "12 months",
                        key: Key(
                            "analytics__tab_analytics_trend_chart_by_month_12")),
                    Tab(
                        text: "Weekday",
                        key: Key(
                            "analytics__tab_analytics_trend_chart_day_of_week"))
                  ],
                ),
                title: Text("Analytics")),
            body: TabBarView(children: [
              AnalyticsTrendChartFourWeek(),
              AnalyticsTrendChartByMonth(6),
              AnalyticsTrendChartByMonth(12),
              AnalyticsTrendChartDayOfWeek(),
            ])));
  }
}
