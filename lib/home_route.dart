import 'package:emote/analytics_route.dart';
import 'package:emote/settings_route.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'checkin_emotion.dart';
import 'checkin_repository.dart';
import 'emotion_repository.dart';

abstract class ListItem {}

class DateHeading implements ListItem {
  final String heading;

  DateHeading(this.heading);

  String formattedDate() {
    var inputFormat = DateFormat("d-M-y");
    var dateTime = inputFormat.parse(this.heading);
    var outputFormat =
        dateTime.year == DateTime.now().year ? "d MMMM" : "d MMMM y";
    return DateFormat(outputFormat).format(dateTime);
  }
}

class CheckinItem implements ListItem {
  final Checkin checkin;

  CheckinItem(this.checkin);

  String formattedTime() {
    var outputFormat = DateFormat.jm("en-US");
    return outputFormat.format(checkin.createdAt.toLocal());
  }
}

class HomeRouteState extends State<HomeRoute> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      extendBodyBehindAppBar: true,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: <Widget>[
          IconButton(
            key: Key("home__analytics_button"),
            icon: Icon(Icons.show_chart,
                semanticLabel: "Analytics",
                color: Theme.of(context).iconTheme.color),
            onPressed: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => AnalyticsRoute())),
          ),
          IconButton(
            key: Key("home__settings_button"),
            icon: Icon(
              Icons.settings,
              semanticLabel: "Settings",
              color: Theme.of(context).iconTheme.color,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingsRoute()));
            },
          )
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Theme.of(context).bottomAppBarColor,
        shape: CircularNotchedRectangle(),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              height: 48,
            )
          ],
        ),
      ),
      body: FutureBuilder<Map<String, List<Checkin>>>(
          future: CheckinRepository().groupedByDate(),
          builder: (BuildContext context,
              AsyncSnapshot<Map<String, List<Checkin>>> snapshot) {
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }
            if (!snapshot.hasData) {
              return new Center(child: new CircularProgressIndicator());
            } else {
              List<ListItem> items = [];

              snapshot.data.forEach((date, checkins) {
                items.add(DateHeading(date));
                items.addAll(checkins.map((ci) => CheckinItem(ci)));
              });

              if (items.isEmpty) {
                return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Transform.rotate(
                            angle: 25,
                            child: Image(
                              image: AssetImage("assets/emotion.png"),
                              width: 72,
                              height: 72,
                            )),
                      ),
                      Text(
                        "Your timeline is empty",
                        style: Theme.of(context).textTheme.headline6,
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        child: Text(
                            "Use the button below to record how you're feeling and it will show up here.",
                            key: Key('placeholder_text'),
                            style: Theme.of(context).textTheme.bodyText2,
                            textAlign: TextAlign.center),
                        padding:
                            EdgeInsets.symmetric(vertical: 16, horizontal: 32),
                      )
                    ]);
              }

              return ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    final item = items[index];

                    if (item is DateHeading) {
                      return ListTile(
                          title: Text(item.formattedDate(),
                              style: Theme.of(context).textTheme.button));
                    } else if (item is CheckinItem) {
                      return FutureBuilder(
                          future: item.checkin.emotion(),
                          builder: (context, AsyncSnapshot<Emotion> snapshot) {
                            if (!snapshot.hasData || snapshot.hasError)
                              return Text("");

                            if (item.checkin.notes != null &&
                                item.checkin.notes.length > 0) {
                              return ExpansionTile(
                                  leading: CircleAvatar(
                                      backgroundColor: snapshot.data.tint() ??
                                          Colors.purpleAccent),
                                  title: Text(snapshot.data.label),
                                  subtitle: Text(item.formattedTime()),
                                  children: <Widget>[
                                    Padding(
                                        child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(item.checkin.notes,
                                                textAlign: TextAlign.left,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText2)),
                                        padding:
                                            EdgeInsets.fromLTRB(86, 0, 16, 16))
                                  ]);
                            } else {
                              return ListTile(
                                  leading: CircleAvatar(
                                      backgroundColor: snapshot.data.tint() ??
                                          Colors.deepPurpleAccent),
                                  title: Text(snapshot.data.label),
                                  subtitle: Text(item.formattedTime()));
                            }
                          });
                    } else {
                      return null;
                    }
                  });
            }
          }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.purpleAccent,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CheckinEmotionRoute(
                      resolver: DatabaseEmotionResolver())));
        },
        tooltip: 'Check in',
        key: Key("fab_checkin"),
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class HomeRoute extends StatefulWidget {
  State<StatefulWidget> createState() {
    return HomeRouteState();
  }
}
