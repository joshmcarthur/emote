import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  Future<bool> isNotPresent(SerializableFinder finder, FlutterDriver driver,
      {Duration timeout = const Duration(seconds: 1)}) async {
    try {
      await driver.waitForAbsent(finder, timeout: timeout);
      return true;
    } catch (e) {
      return false;
    }
  }

  group('Emote Smoketest', () {
    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('check flutter driver health', () async {
      final health = await driver.checkHealth();
      expect(health.status, HealthStatus.ok);
    });

    test("Shows empty list content", () async {
      await driver.runUnsynchronized(() async => expect(
          await driver.getText(find.byValueKey('placeholder_text')),
          "Use the button below to record how you're feeling and it will show up here."));
    });

    test("Shows the default primary emotions", () async {
      await driver.tap(find.byValueKey("fab_checkin"));
      expect(await driver.getText(find.text("Love")), "Love");
      expect(await driver.getText(find.text("Joy")), "Joy");
      expect(await driver.getText(find.text("Surprise")), "Surprise");
      expect(await driver.getText(find.text("Anger")), "Anger");
      expect(await driver.getText(find.text("Sadness")), "Sadness");
      expect(await driver.getText(find.text("Fear")), "Fear");
    });

    test("Traverses to a selection of secondary emotions", () async {
      expect(await driver.getText(find.text("Sadness")), "Sadness");

      await driver.tap(find.text("Fear"));
      expect(await driver.getText(find.text("Horror")), "Horror");

      await driver.tap(find.text("Sadness"));
      expect(await driver.getText(find.text("Sympathy")), "Sympathy");

      await driver.tap(find.text("Surprise"));
      expect(await driver.getText(find.text("Amazement")), "Amazement");
    });

    test("Traverses to a selection of tertiary emotions", () async {
      await driver.tap(find.text("Fear"));
      await driver.tap(find.text("Horror"));
      expect(await driver.getText(find.text("Alarm")), "Alarm");

      await driver.tap(find.text("Sadness"));
      await driver.tap(find.text("Sympathy"));
      expect(await driver.getText(find.text("Pity")), "Pity");
    });

    test("Checks in with a primary emotion", () async {
      await driver.tap(find.text("Surprise"));
      await driver.tap(find.byValueKey("checkin_emotion_submit"));
      await driver.tap(find.byValueKey("checkin_supplementary_submit"));

      expect(
          await isNotPresent(
              find.text(
                  "Use the button below to record how you're feeling and it will show up here."),
              driver),
          true);
      expect(await driver.getText(find.text("Surprise")), "Surprise");
    });

    test("Checks in with a secondary emotion", () async {
      await driver.tap(find.byValueKey("fab_checkin"));

      await driver.tap(find.text("Fear"));
      await driver.tap(find.text("Horror"));
      await driver.tap(find.byValueKey("checkin_emotion_submit"));
      await driver.tap(find.byValueKey("checkin_supplementary_submit"));

      expect(await driver.getText(find.text("Surprise")), "Surprise");
      expect(await driver.getText(find.text("Horror")), "Horror");
    });

    test("Checks in with a tertiary emotion", () async {
      await driver.tap(find.byValueKey("fab_checkin"));

      await driver.tap(find.text("Love"));
      await driver.tap(find.text("Affection"));
      await driver.tap(find.text("Sentimentality"));
      await driver.tap(find.byValueKey("checkin_supplementary_submit"));

      expect(await driver.getText(find.text("Surprise")), "Surprise");
      expect(await driver.getText(find.text("Horror")), "Horror");
      expect(
          await driver.getText(find.text("Sentimentality")), "Sentimentality");
    });

    test("Checks in with notes", () async {
      const notes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";

      await driver.tap(find.byValueKey("fab_checkin"));
      await driver.tap(find.text("Surprise"));
      await driver.tap(find.byValueKey("checkin_emotion_submit"));
      await driver.tap(find.byValueKey("checkin_supplementary_notes"));
      await driver.enterText(notes);

      await driver.tap(find.byValueKey("checkin_supplementary_submit"));
      await driver.tap(find.byType("ExpansionTile"));

      expect(await driver.getText(find.text(notes)), notes);
    });

    test("Settings", () async {
      await driver.tap(find.byValueKey("home__settings_button"));
      expect(await driver.getText(find.text("Export")), "Export");
      expect(await driver.getText(find.text("Reminders")), "Reminders");
      expect(await driver.getText(find.text("About Emote")), "About Emote");
    });

    test("Analytics", () async {
      await driver.tap(find.byTooltip('Back'));
      await driver.tap(find.byValueKey("home__analytics_button"));
      expect(
          find.byValueKey("analytics_route__four_week_trend_chart"), isNotNull);
      expect(
          find.byValueKey("analytics__chart_analytics_trend_chart_by_month_6"),
          isNotNull);
      expect(
          find.byValueKey("analytics__chart_analytics_trend_chart_by_month_12"),
          isNotNull);
      expect(
          find.byValueKey("analytics__chart_analytics_trend_chart_by_month_12"),
          isNotNull);
      expect(
          find.byValueKey("analytics__tab_analytics_trend_chart_day_of_week"),
          isNotNull);
    });
  });
}
